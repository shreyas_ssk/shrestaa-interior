require('dotenv/config');
const server = require('./server');
const express = require('express');
const auth = require('./routes/auth');
const session = require('express-session');
const jwt = require('jsonwebtoken');

const app = express();
app.use('/api-admin', require('./routes/admin-bro'));

app.use(server.bodyParser.json());
app.use(server.bodyParser.urlencoded({
    extended: true
}));

app.use(session({
    secret: "Our little secrete.",
    resave: false,
    saveUninitialized: false
}));


server.mongoose.connect(process.env.final_Mongo, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}, () => console.log("Database connected!"));

server.mongoose.set("useCreateIndex", true);

app.set('view engine', 'ejs');
app.use('/uploads', express.static('uploads'));


app.use('/', require('./routes/login'));

app.get('/shreyas', (req, res) => {
    res.send("<h1>kempudi-renedir<h1> <br> <h1>Sharon Prabhu Ji</h1>")
})

// app.post('/test', auth, (req, res) => {
//     jwt.verify(req.token, process.env.TOKEN_SECRETE, (err, authData) => {
//         if(err) {
//             res.sendStatus(403);
//         } else {
//             res.json({
//                 message: "Post created!",
//                 authData
//             });
//         }
//     })
// });

app.use("/kitchen", auth, require('./routes/kitchen'));
app.use("/dine", auth, require('./routes/dining'));
app.use("/main", auth, require('./routes/main'));
app.use("/bed", auth, require('./routes/bed'));
app.use("/foyer", auth, require('./routes/foyer'));
app.use("/commercial", auth, require('./routes/commercial'));


const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
    console.log(`Server listening of port ${PORT}...`)
});