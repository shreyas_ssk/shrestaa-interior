const server = require('../server');
const Model = require('../models/database-model');
const express = require('express');
const router = express.Router();

const Db = Model.Bed; //Change here

router.get("/", (req, res) => {
    Db.find((err, foundBlog) => {
        if(!err) {
            res.send(foundBlog);
        } else {
            res.send(err);
        }
    });
});


router.post("/", (req, res) => {

    const post = new Db({
        title: req.body.title,
        content: req.body.content,
        category: req.body.category,
        client: req.body.client,
        date: req.body.date,
        image: req.body.image
    });

    post.save(function (err) {
        if (!err) {
            console.log("Successfully posted to the server!");
        } else {
            console.log(err);
        }
    });
});


router.post("/delete", (req, res) => {
    const checkedItem = req.body.checkeditem;

    Db.findOneAndDelete({_id: server.ObjectID(checkedItem)}, (err) => {
        if (!err) {
            console.log("Successfully deleted Item");
        } else {
            console.log(err);
        }
    });
});


module.exports = router;