const server = require('../server');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');


const app = express();


app.use(server.bodyParser.urlencoded({ extended: true }));
app.use(server.bodyParser.json());


const users = {
      id: '123456776452314',
      email: process.env.ADMIN_EMAIL,
      password: process.env.ADMIN_PASSWORD
  }


router.get("/", (req, res) => {
    res.render("./login");
});

router.post("/", (req, res) => {
  
      jwt.sign(users, process.env.TOKEN_SECRETE, (err, token) => {
        res.json({
          token
        });
      });
});

module.exports = router;