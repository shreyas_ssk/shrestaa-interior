const AdminBro = require('admin-bro');
const AdminBroExpress = require('@admin-bro/express');
const AdminBroMongoose = require('@admin-bro/mongoose');
const mongoose = require('mongoose');

AdminBro.registerAdapter(AdminBroMongoose);

const Db = require('../models/database-model');

const adminBro = new AdminBro({
  databases: [mongoose],
  resources: [
  {
    resource: Db.Kitchen
  },
  {
    resource: Db.Dine
  },
  {
    resource: Db.Main
  },
  {
    resource: Db.Bed
  },
  {
    resource: Db.Foyer
  },
  {
    resource: Db.Commercial
  }
], 
  branding: {
      companyName: 'Shrestaa Interior'
  },
  rootPath: '/api-admin',
  logoutPath: '/api-admin/exit',
  loginPath: '/api-admin/sign-in',
});

const ADMIN = {
  email: process.env.ADMIN_EMAIL,
  password: process.env.ADMIN_PASSWORD  
}

const router = AdminBroExpress.buildAuthenticatedRouter(adminBro, {
  cookieName: process.env.ADMIN_COOKIE_NAME || 'admin-bro',
  cookiePassword: process.env.ADMIN_COOKIE_PASS || 'supersecrete-and-long-password-for-a-cookie-in-the-browser',
  authenticate: async (email, password) => {
    if (email === ADMIN.email && password === ADMIN.password) {
      return ADMIN
    } else {
      console.log("wrong creds!")
    }
  }
});

module.exports = router;