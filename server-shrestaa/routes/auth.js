const jwt = require('jsonwebtoken');

module.exports = function (req, res, next) {
    const bearerHeader = req.headers['authorization'];

    if (typeof bearerHeader != 'undefined') {
        const bearer = bearerHeader.split(' ');
        const bearerToken = bearer[1];
        req.token = bearerToken;
        next();
    }
}



// function (req, res, next) {
//     const token = req.header('auth-token');
//     if (!token) return res.status(401).send('Access Denied!');

//     try {
//         const verified = jwt.verify(token, process.env.TOKEN_SECRETE);
//         req.user = verified;
//         next()
//     } catch {
//         res.status(400).send('Invalid Token!')
//     }
// };