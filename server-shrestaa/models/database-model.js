const mongoose = require('mongoose');
const base_schema = {
    title: {
        type: String,
        required: true,
    },
    content: {
        type: String,
        required: true,
    },
    category: {
        type: String,
        required: true,
    },
    client: {
        type: String,
        required: true,
    },
    image: {
        type: Array,
        required: true,
    },
    date: {
        type: String,
        required: true
    }
}


const KitchenSchema = new mongoose.Schema(base_schema);
const DineSchema = new mongoose.Schema(base_schema);
const MainSchema = new mongoose.Schema(base_schema);
const BedSchema = new mongoose.Schema(base_schema);
const FoyerSchema = new mongoose.Schema(base_schema);
const CommercialSchema = new mongoose.Schema(base_schema);

const Kitchen = mongoose.model('Kitchen', KitchenSchema);
const Dine = mongoose.model('Dine', DineSchema);
const Main = mongoose.model('Main', MainSchema);
const Bed = mongoose.model('Bed', BedSchema);
const Foyer = mongoose.model('Foyer', FoyerSchema);
const Commercial = mongoose.model('Commercial', CommercialSchema);


module.exports = {
    Kitchen: Kitchen,
    Dine: Dine,
    Main: Main,
    Bed: Bed,
    Foyer: Foyer,
    Commercial: Commercial
}
