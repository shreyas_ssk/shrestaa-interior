const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const path = require('path');
const multer = require('multer');
const fs = require('fs');
const ObjectID = require('mongodb').ObjectID;
const http = require("http");

module.exports = {
    bodyParser: bodyParser,
    mongoose: mongoose,
    path: path,
    multer: multer,
    fs: fs,
    ObjectID: ObjectID,
    http: http
}