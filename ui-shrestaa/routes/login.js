const express = require('express');
const router = express.Router();
const passport = require('passport');

const users = [
    {
        id: '123456776452314',
        email: 'shrestaainterior@gmail.com',
        password: 'Shreyas123'
    }
]

router.get("/", (req, res) => {
    res.render("./admin/login");
});

const ADMIN = {
    email: process.env.ADMIN_EMAIL,
    password: process.env.ADMIN_PASSWORD
};

router.post("/", (req, res) => {

    const { email, password } = req.body

    if (email && password) {
        const user = users.find(user => user.email === email && user.password === password)

        if(user) {
            req.session.userId = user.id
            return res.redirect('/admin');
        }
    }

    res.redirect('/login')

})

module.exports = router;