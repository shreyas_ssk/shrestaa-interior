const server = require('../server');
const express = require('express');
const router = express.Router();

const app = express();

const url = process.env.final_URL;

router.get('/', (req, res) => {
    const { userId } = req.session
    res.render("./admin/home");
});

router.use("/kitchen", require('./kitchen'));
router.use("/dine", require('./dining'));
router.use("/main", require('./main'));
router.use("/bed", require('./bed'));
router.use("/foyer", require('./foyer'));
router.use("/commercial", require('./commercial'));


module.exports = router;