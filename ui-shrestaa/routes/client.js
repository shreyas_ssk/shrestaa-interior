const express = require('express');
const router = express.Router();
const server = require('../server');
const fetch = require('node-fetch');

express().use(server.bodyParser.urlencoded({extended: false}));
express().use(server.bodyParser.json());

router.get("/", (req, res) => {
    res.render("./home");
});

const url = process.env.final_URL;

router.get("/kitchen-room", (req, res) => {
    fetch(url, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(data => {
        kData = data;
        fetch(url + 'kitchen', {
            headers: {
                'Authorization': 'Bearer ' + kData.token
            }
        })
        .then(response => response.json())
        .then(data => {
            kData = data;
            res.render("./kitchen", {
                data: kData,
                kUrl: url
            })
        });
    });
});

router.get("/dining-area", (req, res) => {
    fetch(url, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(data => {
        kData = data;
        fetch(url + 'dine', {
            headers: {
                'Authorization': 'Bearer ' + kData.token
            }
        })
        .then(response => response.json())
        .then(data => {
            kData = data;
            res.render("./dining", {
                data: kData,
                kUrl: url
            })
        });
    });
});

router.get("/living-area", (req, res) => {
    fetch(url, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(data => {
        kData = data;
        fetch(url + 'main', {
            headers: {
                'Authorization': 'Bearer ' + kData.token
            }
        })
        .then(response => response.json())
        .then(data => {
            kData = data;
            res.render("./main", {
                data: kData,
                kUrl: url
            })
        });
    });
});

router.get("/bed-room", (req, res) => {
    fetch(url, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(data => {
        kData = data;
        fetch(url + 'bed', {
            headers: {
                'Authorization': 'Bearer ' + kData.token
            }
        })
        .then(response => response.json())
        .then(data => {
            kData = data;
            res.render("./bed", {
                data: kData,
                kUrl: url
            })
        });
    });
});

router.get("/foyer", (req, res) => {
    fetch(url, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(data => {
        kData = data;
        fetch(url + 'foyer', {
            headers: {
                'Authorization': 'Bearer ' + kData.token
            }
        })
        .then(response => response.json())
        .then(data => {
            kData = data;
            res.render("./foyer", {
                data: kData,
                kUrl: url
            })
        });
    });
});

router.get("/commercial-space", (req, res) => {
    fetch(url, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(data => {
        kData = data;
        fetch(url + 'commercial', {
            headers: {
                'Authorization': 'Bearer ' + kData.token
            }
        })
        .then(response => response.json())
        .then(data => {
            kData = data;
            res.render("./commercial", {
                data: kData,
                kUrl: url
            })
        });
    });
});

module.exports = router;