const server = require('../server');
const express = require('express');
const router = express.Router();
const fetch = require('node-fetch');

const url = process.env.final_URL; //Change Here

const storage = server.multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/');
    },
    filename: (req, file, cb) => {
        cb(null, file.fieldname + '-' + Date.now() + server.path.extname(file.originalname));
    }
});

const fileFilter = (req, file, cb) => {
    //regect a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(new Error('message: jpeg, jpg and png accepted only'), false);
    }
};

var upload = server.multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 10
    },
    fileFilter: fileFilter,
});

router.get("/", (req, res) => {
    fetch(url, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(data => {
        kData = data;
        fetch(url + 'main', {
            headers: {
                'Authorization': kData.token
            }
        })
        .then(response => response.json())
        .then(data => {
            kData = data;
            res.render("./admin/main", {
                data: kData,
                kUrl: url
            })
        });
    });
});


router.post("/", upload.array('image', 5), (req, res) => {

    const body = {
        title: req.body.kTitle,
        content: req.body.kContent,
        category: req.body.kCat,
        client: req.body.kClient,
        date: req.body.kDate,
        image: req.files.map(file => file.path)
    };

    fetch(url, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(data => {
        kData = data;
        fetch(url + 'main', { //Change Here
            method: 'post',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': kData.token
            },
        })
        .then(res => res.json())
    });

    res.redirect("/admin/main"); //Change Here
});


router.post("/delete", (req, res) => {

    const body = {
        checkeditem: req.body.kButton
    }

    const checkedImage = req.body.dImage;
    let temp = checkedImage.split(",");
    for (i = 0; i < temp.length; i++) {
        server.fs.unlink(temp[i], (err) => {
            console.log(err);
        });
    };

    fetch(url, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(data => {
        kData = data;
        fetch(url + 'main/delete', {
            method: 'post',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                'Authorization': kData.token
            },
        })
        .then(res => res.json())
    });

    res.redirect("/admin/main"); //Change Here
});


module.exports = router;