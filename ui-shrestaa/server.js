const bodyParser = require('body-parser');
const path = require('path');
const multer = require('multer');
const fs = require('fs');
const http = require("http");

module.exports = {
    bodyParser: bodyParser,
    path: path,
    multer: multer,
    fs: fs,
    http: http
}