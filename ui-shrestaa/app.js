require('dotenv/config');
const express = require("express");
const server = require('./server');
const session = require('express-session');
const passport = require('passport');
const fetch = require('node-fetch');


const users = [
    {
        id: '123456776452314',
        email: process.env.EMAIL,
        password: process.env.PASSWORD
    }
]

const app = express();

app.set('view engine', 'ejs');
app.use(server.bodyParser.urlencoded({
    extended: true
}));
app.use(server.bodyParser.json());
app.use(express.static("assets"));
app.use('/uploads', express.static('uploads'));

const sessionName = 'shrestaa-server';


app.use(session({
    name: sessionName,
    resave: false,
    saveUninitialized: false,
    secret: process.env.AUTH_TOKEN,
    cookie: {
        maxAge: 1000 * 60 * 60 * 2,
        sameSite: true,
    }
}));

const redirectLogin = (req, res, next) => {
    if(!req.session.userId) {
        res.redirect('/login')
    } else {
        next()
    }
}

const redirectHome = (req, res, next) => {
    if(req.session.userId) {
        res.redirect('/admin')
    } else {
        next()
    }
}

app.use(passport.initialize());
app.use(passport.session());


app.use("/", require('./routes/client'));
app.use("/login", redirectHome, require('./routes/login'));

app.get('/test', (req, res) => {

    fetch(url, {
        method: 'post',
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(res => res.json())
    .then(data => {
        kData = data;
    });

    
})


app.use("/admin", redirectLogin, require('./routes/admin'));

app.post('/logout', redirectLogin, (req, res) => {
    req.session.destroy(err => {
        if (err) {
            return res.redirect('/admin')
        }

        res.clearCookie(sessionName);
        res.redirect('/login');
    })
})

const PORT = 3000;
app.listen(PORT, function () {
    console.log(`Server started on port ${PORT}`);
});